package com.sh.utils.file.nginx;

import java.text.SimpleDateFormat;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.sh.utils.file.SecretUtil;


/**
  * 类名：NginxUtil.java
  * 类说明： nginx帮助类
  * Copyright: Copyright (c) 2012-2019
  * Company: HT
  * @author     ht-haoxiuzhu
  * @date       2019年9月16日
  * @version    1.0
*/
public class NginxErrorUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(NginxErrorUtil.class);
    public static final String SDF_DATE_TIME_MINUTE_SECONDS_MM = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String SDF_DATE_TIME_MM_NGINX = "yyyy/MM/dd HH:mm:ss.SSS";
    public static final String SDF_DATE_TIME_MINUTE_SECONDS = "yyyy-MM-dd HH:mm:ss";
    public static final String SDF_DATE_TIME_NGINX = "yyyy/MM/dd HH:mm:ss";
    public static final String STR_BLANK = " ";
    /**
      * 方法：getAccessLog 
      * 方法说明：    获取access日志信息
      * @param lineStr 行数据字符串
      * @author     ht-haoxiuzhu
      * @date       2019年9月16日
     */
    public static JSONObject getAccessLog(String lineStr) {
//    	LOGGER.info(lineStr);
        if(lineStr != null){
        	JSONObject json = new JSONObject();
        	try {
        		if(lineStr.length()>19) {
                 	String dateTime = lineStr.substring(0, 19);
                 	String dateTimeFormat = null;
                    if(dateTime.indexOf(".")!=-1) {
                 	   dateTimeFormat =  getFormatMillStr(dateTime);
                    }else {
                 	   dateTimeFormat =  getFormatStr(dateTime);
                    }
                    json.put("dateTime",dateTimeFormat);
                    if(dateTimeFormat != null && dateTimeFormat.length()>10){
                        json.put("dateDay",dateTimeFormat.subSequence(0, 10));
                    }
                    String[] strSplits  = lineStr.substring(19).trim().split(", ");
                    for(String str:strSplits) {
                     	String[] strS  = str.split(":");
                     	if(strS.length>1) {
                     		if("request".equals(strS[0])) {
                     			String[] strSps  = strS[1].trim().split(STR_BLANK);
                     			if(strSps.length>2) {
                     				String mothod =  strSps[0].indexOf("GET")!=-1?"GET":"POST";
                     				json.put("mothod", mothod);
                     				json.put("status", "200");
                     				String userAgent = strSps[2].replace("\"", "");//无userAgent 使用http/1.1填充
                                    json.put("userAgent", userAgent);
                         			String request = SecretUtil.urlDecode(strSps[1].trim());//针对终端GET请求参数值未URLEncode加密case
//                                 	request = SecretUtil.urlDecodeTrunk(strSps[1].trim());//新版本请求参数值已URLEncode加密
                     				if("GET".equals(mothod)){
                                        NginxGetUtil.getRequestParam(json,request);
                                    }
                         			
                     		}
                     		if("client".equals(strS[0])) {
                     			String ip = strS[1].trim();
                     			json.put("ip", ip);
                     		}
                     	}
                     }
                 }
            	}
        	}catch(Exception e) {
        		LOGGER.error("getAccessLog has error!",e);
        	}
           return json;
        }
        return null;
    }
    /** 
      * 方法：getFormatStr 
      * 方法说明：    获取格式化日期时间字符串
      * @param dateTime
      * @return
      * @author     ht-haoxiuzhu
      * @date       2019年9月17日
    */
    private static String getFormatStr(String dateTime) {
        if(dateTime != null){
            try{
                SimpleDateFormat sdf_nginx = new SimpleDateFormat(SDF_DATE_TIME_NGINX, Locale.US);
                SimpleDateFormat sdf = new SimpleDateFormat(SDF_DATE_TIME_MINUTE_SECONDS);
                return sdf.format(sdf_nginx.parse(dateTime));
            }catch(Exception e){
                LOGGER.error("getFormatStr has error!",e);
            }
        }
        return null;
    }
    /** 
     * 方法：getFormatMillStr 
     * 方法说明：    获取格式化日期时间字符串--毫秒
     * @param dateTime
     * @return
     * @author     ht-haoxiuzhu
     * @date       2019年9月17日
   */
   private static String getFormatMillStr(String dateTime) {
       if(dateTime != null){
           try{
               SimpleDateFormat sdf_nginx = new SimpleDateFormat(SDF_DATE_TIME_MM_NGINX, Locale.US);
               SimpleDateFormat sdf = new SimpleDateFormat(SDF_DATE_TIME_MINUTE_SECONDS_MM);
               return sdf.format(sdf_nginx.parse(dateTime));
           }catch(Exception e){
               LOGGER.error("getFormatStr has error!",e);
           }
       }
       return null;
   }
   public static void main(String[] args) {
	   String logStr = "1.119.149.74 - - [23/Sep/2020:11:26:43 +0800] \"GET /tpGather/a.html?guid=70957002-208b-374a-5b6e-717370000003&appVersion=0.1.4&appPackage=com.sh.project.push.service&localtime=20200923112643263&eventType=18&chname=%E7%BA%A2%E6%B5%B7%E8%A1%8C%E5%8A%A8&packageName=com.gitvvideo.zhujiangshuma&action=com.gitvvideo.zhujiangshuma.action.ACTION_DETAIL&startflag=1&params=eyJhcHBOYW1lIjoi54ix5aWH6Im6IiwianVtcEluZm8iOnsiYWN0aW9uIjoiY29tLmdpdHZ2aWRl%0Aby56aHVqaWFuZ3NodW1hLmFjdGlvbi5BQ1RJT05fREVUQUlMIiwiZG9XaGF0Ijoic3RhcnRBY3Rp%0Adml0eSIsInBhY2thZ2VOYW1lIjoiY29tLmdpdHZ2aWRlby56aHVqaWFuZ3NodW1hIiwicGFyYW1z%0AIjoie1wicGxheUluZm9cIjpcIntcXFwidmlkZW9JZFxcXCI6XFxcIjczMzg5NTUwMFxcXCIsXFxc%0AImVwaXNvZGVJZFxcXCI6XFxcIjczMzg5NTUwMFxcXCIsXFxcImNobklkXFxcIjpcXFwiMVxcXCIs%0AXFxcImN1c3RvbWVyXFxcIjpcXFwiemh1amlhbmdzaHVtYVxcXCJ9XCJ9In0sIm5hbWUiOiLnuqLm%0AtbfooYzliqgifQ%3D%3D HTTP/1.1\" 200 0 \"-\" \"okhttp/3.11.0 (Linux; U; Android 4.4.2; Hi3798MV200; 20508;) Version/2.0.2\"";
	   logStr= "2020/09/23 10:33:45 [error] 22709#0: *343389 directory index of \"/usr/local/nginx/html/ics/\" is forbidden, client: 173.14.69.161, server: localhost, request: \"GET /tpGather/a.html?guid=70957002-208b-374a-5b6e-717370000003&appVersion=0.1.4&appPackage=com.sh.project.push.service&localtime=20200923112643263&eventType=18&chname=%E7%BA%A2%E6%B5%B7%E8%A1%8C%E5%8A%A8&packageName=com.gitvvideo.zhujiangshuma&action=com.gitvvideo.zhujiangshuma.action.ACTION_DETAIL&startflag=1&params=eyJhcHBOYW1lIjoi54ix5aWH6Im6IiwianVtcEluZm8iOnsiYWN0aW9uIjoiY29tLmdpdHZ2aWRl%0Aby56aHVqaWFuZ3NodW1hLmFjdGlvbi5BQ1RJT05fREVUQUlMIiwiZG9XaGF0Ijoic3RhcnRBY3Rp%0Adml0eSIsInBhY2thZ2VOYW1lIjoiY29tLmdpdHZ2aWRlby56aHVqaWFuZ3NodW1hIiwicGFyYW1z%0AIjoie1wicGxheUluZm9cIjpcIntcXFwidmlkZW9JZFxcXCI6XFxcIjczMzg5NTUwMFxcXCIsXFxc%0AImVwaXNvZGVJZFxcXCI6XFxcIjczMzg5NTUwMFxcXCIsXFxcImNobklkXFxcIjpcXFwiMVxcXCIs%0AXFxcImN1c3RvbWVyXFxcIjpcXFwiemh1amlhbmdzaHVtYVxcXCJ9XCJ9In0sIm5hbWUiOiLnuqLm%0AtbfooYzliqgifQ%3D%3D HTTP/1.1\" 200 0 \"-\" \"okhttp/3.11.0 (Linux; U; Android 4.4.2; Hi3798MV200; 20508;) Version/2.0.2\", host: \"123.57.209.234:81\""; 
	   System.out.println(getAccessLog(logStr).toJSONString());
   }
}
