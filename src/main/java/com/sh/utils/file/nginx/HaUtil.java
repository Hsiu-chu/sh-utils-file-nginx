package com.sh.utils.file.nginx;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.sh.utils.file.SecretUtil;


/**
  * 类名：HaUtil.java
  * 类说明： ha帮助类
  * Copyright: Copyright (c) 2012-2019
  * Company: HT
  * @author     ht-haoxiuzhu
  * @date       2019年9月16日
  * @version    1.0
*/
public class HaUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(HaUtil.class);
    public static final String SDF_DATE_TIME_MINUTE_SECONDS_MM = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String SDF_DATE_TIME_MM_NGINX = "dd/MMM/yyyy:HH:mm:ss.SSS";
    public static final String SDF_DATE_TIME_MINUTE_SECONDS = "yyyy-MM-dd HH:mm:ss";
    public static final String SDF_DATE_TIME_NGINX = "dd/MMM/yyyy:HH:mm:ss";
    /**
      * 方法：getAccessLogPlus 
      * 方法说明：   正则解析并获取access日志信息
      * @param lineStr
      * @return
      * @author     haoxiuzhu
      * @date       Sep 23, 2020
     */
    public static JSONObject getAccessLogPlus(String lineStr) {
        if(lineStr != null){
           try{
//        	   String a = "116.199.7.37 - - [12/Jan/2021:15:59:59.999 +0800] \"GET /launcherGather/a.html?guid=70927002-6c2c-dca6-6831-718320100529&areaCode=Y01P0002&appVersion=0.4.4&rom=20508&appPackage=com.skyworthdigital.launcher&runtime=120&localtime=20210112155959945&isTest=0&eventType=14&eventMode=switch&style=1&name=%E6%99%AE%E9%80%9A%E6%A1%8C%E9%9D%A2&nid=b00a24b28f4a4dd4860432012d8dfdd8&nname=%E7%9B%B4%E6%92%AD&ncode=live&pid=a82215e476854093ab7169386beb0377&pname=%E7%9B%B4%E6%92%AD%E9%A6%96%E5%B1%8F&ptitle=%E7%9B%B4%E6%92%AD%E9%A6%96%E5%B1%8F&seq=6&iid=76e457343eaf4498847f42ffdec6bde9&iname=%E4%B8%83%E5%A4%A9%E5%9B%9E%E7%9C%8B&ititle=&itype=1&opmode=0&datasource=1 HTTP/1.1\" 200 11 \"-\" \"okhttp/3.11.0\" \"10.170.144.57\"";
//        	   String lineStr = "Jan 11 10:50:00 localhost haproxy[7722]: 10.226.69.82:41530 [11/Jan/2021:10:50:00.015] PICM launcher_ics/192.168.44.138 8/0/1/0/9 200 151 - - ---- 17198/17198/1/1/0 0/0 \"GET /launcherGather/a.html?guid=70587002-0c49-33f0-f47c-718480075926&areaCode=Y01P0007&appVersion=0.4.4&rom=20506&appPackage=com.skyworthdigital.launcher&runtime=2070644&localtime=20210111104959132&isTest=0&eventType=1&eventMode=heartbeat&runstate=bg&style=1&name=%E9%BB%84%E5%9F%94%E6%99%AE%E9%80%9A%E6%A1%8C%E9%9D%A2 HTTP/1.1\"";
        	     
        	   Pattern pattern = Pattern.compile(
       				"([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) (\\[.*\\]) ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*)");
       	      Matcher matcher = pattern.matcher(lineStr);
              JSONObject json = new JSONObject();
              if (matcher.find()) {
           	   String ip = getMatcherGroup(matcher,6);//1.119.149.74:41530
           	   	  
                  json.put("ip", ip.substring(0, ip.indexOf(":")));
                  
                  String localTime = getMatcherGroup(matcher,7);//[23/Sep/2020:11:26:43 +0800]
                  String dateTime = getTimeByLocalTime(localTime);
                  String dateTimeFormat = null;
                  if(dateTime.indexOf(".")!=-1) {
               	   dateTimeFormat =  getFormatMillStr(dateTime);
                  }else {
               	   dateTimeFormat =  getFormatStr(dateTime);
                  }
                  json.put("dateTime",dateTimeFormat);
                  if(dateTimeFormat != null && dateTimeFormat.length()>10){
                      json.put("dateDay",dateTimeFormat.subSequence(0, 10));
                  }
                  
                  String status =  getMatcherGroup(matcher,11);//200
                  json.put("status", status);
                  
//                  String userAgent = getMatcherGroup(matcher,9);//"okhttp/3.11.0 (Linux; U; Android 4.4.2; Hi3798MV200; 20508;) Version/2.0.2"
//                  json.put("userAgent", userAgent.replace("\"", ""));
                  String mothod = getMatcherGroup(matcher,18).replace("\"", "");
                  json.put("mothod", mothod);
                  String request = getMatcherGroup(matcher,19);//"GET /tpGather/a.html?key=value&key1=value1 HTTP/1.1"
//                          json.put("request",request);
                  if("GET".equals(mothod)){
                      NginxGetUtil.getRequestParam(json,request);
                  }
              }
              return json;
           }catch(Exception e) {
        	   LOGGER.error("getAccessLogPlus has error!",e);
           }
        }
        return null;
    }
    /**
      * 方法：getArrayByPath 
      * 方法说明：    根据path获取对应的分隔数组
      * @param path
      * @return
      * @author     haoxiuzhu
      * @date       Sep 23, 2020
     */
    public static String[] getArrayByPath(String path) {
    	String[] paths= new String[3];
	    if(path != null) {
			Pattern pattern = Pattern.compile(
					"\"(\\w+) ([^\\\"]*) ([^\\\"]*)\"");
			Matcher matcher = pattern.matcher(path);
			if (matcher.find()) {
				paths[0]= getMatcherGroup(matcher,1);
				paths[1]= getMatcherGroup(matcher,2);
				paths[2]= getMatcherGroup(matcher,3);
			}
		}
		return paths;
    }
    /**
      * 方法：getTimeByLocalTime 
      * 方法说明：    根据localTime获取dateTime
      * @param localTime [23/Sep/2020:11:26:43 +0800]
      * @return 
      * @author     haoxiuzhu
      * @date       Sep 23, 2020
     */
    private static String getTimeByLocalTime(String localTime) {
		if(localTime != null) {
			Pattern pattern = Pattern.compile(
					"\\[(\\S+)\\]");
			Matcher matcher = pattern.matcher(localTime);
			if (matcher.find()) {
				return getMatcherGroup(matcher,1);
			}
		}
		return null;
	}
	/**
      * 方法：getMatcherGroup 
      * 方法说明：    获取正则group值 报错返回null
      * @param matcher
      * @param group
      * @return
      * @author     haoxiuzhu
      * @date       Sep 23, 2020
     */
	private static String getMatcherGroup(Matcher matcher, int group) {
		try {
			return matcher.group(group);
		}catch(Exception e) {
			LOGGER.error("getGroupValue has error!",e);
		}
		return null;
	}
    /** 
      * 方法：getFormatStr 
      * 方法说明：    获取格式化日期时间字符串
      * @param dateTime
      * @return
      * @author     ht-haoxiuzhu
      * @date       2019年9月17日
    */
    private static String getFormatStr(String dateTime) {
        if(dateTime != null){
            try{
                SimpleDateFormat sdf_nginx = new SimpleDateFormat(SDF_DATE_TIME_NGINX, Locale.US);
                SimpleDateFormat sdf = new SimpleDateFormat(SDF_DATE_TIME_MINUTE_SECONDS);
                return sdf.format(sdf_nginx.parse(dateTime));
            }catch(Exception e){
                LOGGER.error("getFormatStr has error!",e);
            }
        }
        return null;
    }
    /** 
     * 方法：getFormatMillStr 
     * 方法说明：    获取格式化日期时间字符串--毫秒
     * @param dateTime
     * @return
     * @author     ht-haoxiuzhu
     * @date       2019年9月17日
   */
   private static String getFormatMillStr(String dateTime) {
       if(dateTime != null){
           try{
               SimpleDateFormat sdf_nginx = new SimpleDateFormat(SDF_DATE_TIME_MM_NGINX, Locale.US);
               SimpleDateFormat sdf = new SimpleDateFormat(SDF_DATE_TIME_MINUTE_SECONDS_MM);
               return sdf.format(sdf_nginx.parse(dateTime));
           }catch(Exception e){
               LOGGER.error("getFormatStr has error!",e);
           }
       }
       return null;
   }
   public static void main(String[] args) {
	   String logStr = "1.119.149.74 - - [23/Sep/2020:11:26:43 +0800] \"GET /tpGather/a.html?guid=70957002-208b-374a-5b6e-717370000003&appVersion=0.1.4&appPackage=com.sh.project.push.service&localtime=20200923112643263&eventType=18&chname=%E7%BA%A2%E6%B5%B7%E8%A1%8C%E5%8A%A8&packageName=com.gitvvideo.zhujiangshuma&action=com.gitvvideo.zhujiangshuma.action.ACTION_DETAIL&startflag=1&params=eyJhcHBOYW1lIjoi54ix5aWH6Im6IiwianVtcEluZm8iOnsiYWN0aW9uIjoiY29tLmdpdHZ2aWRl%0Aby56aHVqaWFuZ3NodW1hLmFjdGlvbi5BQ1RJT05fREVUQUlMIiwiZG9XaGF0Ijoic3RhcnRBY3Rp%0Adml0eSIsInBhY2thZ2VOYW1lIjoiY29tLmdpdHZ2aWRlby56aHVqaWFuZ3NodW1hIiwicGFyYW1z%0AIjoie1wicGxheUluZm9cIjpcIntcXFwidmlkZW9JZFxcXCI6XFxcIjczMzg5NTUwMFxcXCIsXFxc%0AImVwaXNvZGVJZFxcXCI6XFxcIjczMzg5NTUwMFxcXCIsXFxcImNobklkXFxcIjpcXFwiMVxcXCIs%0AXFxcImN1c3RvbWVyXFxcIjpcXFwiemh1amlhbmdzaHVtYVxcXCJ9XCJ9In0sIm5hbWUiOiLnuqLm%0AtbfooYzliqgifQ%3D%3D HTTP/1.1\" 200 0 \"-\" \"okhttp/3.11.0 (Linux; U; Android 4.4.2; Hi3798MV200; 20508;) Version/2.0.2\"";
	   System.out.println(getAccessLogPlus(logStr).toJSONString());
   }
}
