package com.sh.utils.file.nginx;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.sh.utils.file.SecretUtil;


/**
  * 类名：NginxUtil.java
  * 类说明： nginx帮助类
  * Copyright: Copyright (c) 2012-2019
  * Company: HT
  * @author     ht-haoxiuzhu
  * @date       2019年9月16日
  * @version    1.0
*/
public class NginxUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(NginxUtil.class);
    public static final String SDF_DATE_TIME_MINUTE_SECONDS_MM = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String SDF_DATE_TIME_MM_NGINX = "dd/MMM/yyyy:HH:mm:ss.SSS";
    public static final String SDF_DATE_TIME_MINUTE_SECONDS = "yyyy-MM-dd HH:mm:ss";
    public static final String SDF_DATE_TIME_NGINX = "dd/MMM/yyyy:HH:mm:ss";
    /**
      * 方法：getAccessLogPlus 
      * 方法说明：   正则解析并获取access日志信息
      * @param lineStr
      * @return
      * @author     haoxiuzhu
      * @date       Sep 23, 2020
     */
    public static JSONObject getAccessLogPlus(String lineStr) {
        if(lineStr != null){
           try{
        	   Pattern pattern = Pattern.compile(
       				"([^ ]*) ([^ ]*) ([^ ]*) (\\[.*\\]) (\\\".*?\\\") (-|[0-9]*) (-|[0-9]*) (\\\".*?\\\") (\\\".*?\\\")");
       	      Matcher matcher = pattern.matcher(lineStr);
              JSONObject json = new JSONObject();
              if (matcher.find()) {
           	   String ip = getMatcherGroup(matcher,1);//1.119.149.74
                  json.put("ip", ip);
                  
                  String localTime = getMatcherGroup(matcher,4);//[23/Sep/2020:11:26:43 +0800]
                  String dateTime = getTimeByLocalTime(localTime);
                  String dateTimeFormat = null;
                  if(dateTime.indexOf(".")!=-1) {
               	   dateTimeFormat =  getFormatMillStr(dateTime);
                  }else {
               	   dateTimeFormat =  getFormatStr(dateTime);
                  }
                  json.put("dateTime",dateTimeFormat);
                  if(dateTimeFormat != null && dateTimeFormat.length()>10){
                      json.put("dateDay",dateTimeFormat.subSequence(0, 10));
                  }
                  
                  String status =  getMatcherGroup(matcher,6);//200
                  json.put("status", status);
                  
                  String userAgent = getMatcherGroup(matcher,9);//"okhttp/3.11.0 (Linux; U; Android 4.4.2; Hi3798MV200; 20508;) Version/2.0.2"
                  json.put("userAgent", userAgent.replace("\"", ""));
                  
                  String path = getMatcherGroup(matcher,5);//"GET /tpGather/a.html?key=value&key1=value1 HTTP/1.1"
                  if(path != null) {
               	   String[] pathArray = getArrayByPath(path);
               	   if(pathArray.length>=3) {
               		   String mothod = pathArray[0];
          				   json.put("mothod", mothod);
              			   String request = null;
                          if(userAgent != null && userAgent.indexOf("Version")!=-1) {
                        	   request = SecretUtil.urlDecodeTrunk(pathArray[1]);//新版本请求参数值已URLEncode加密
                        	   getUserAgentParam(json, userAgent);
                          }else {
                        	   request = SecretUtil.urlDecode(pathArray[1]);//针对终端GET请求参数值未URLEncode加密case
                          }
//                          json.put("request",request);
                          if("GET".equals(mothod)){
                              NginxGetUtil.getRequestParam(json,request);
                          }
               	   }
                  }
              }
              return json;
           }catch(Exception e) {
        	   LOGGER.error("getAccessLogPlus has error!",e);
           }
        }
        return null;
    }
    /**
      * 方法：getUserAgentParam 
      * 方法说明：获取userAgent参数
      * @param json
      * @param userAgent "okhttp/3.11.0 (Linux; U; Android 4.4.2; Hi3798MV200; 20508;) Version/2.0.2"
      * @author     haoxiuzhu
      * @date       Mar 16, 2022
     */
    public static void getUserAgentParam(JSONObject json, String userAgent) {
        if(userAgent != null){
            String[] devs = getDevsByUserAgent(userAgent);
            if(devs.length>=2) {
                json.put("model", devs[0]);
                json.put("rom", devs[1]);
            }
        }
    }
    /**
      * 方法：getDevsByUserAgent 
      * 方法说明：    从userAgent里获取设备信息数组 model和rom
      * @param userAgent
      * @return
      * @author     haoxiuzhu
      * @date       Mar 16, 2022
     */
    private static String[] getDevsByUserAgent(String userAgent) {
        String[] devs = new String[2];
        if(userAgent != null) {
            Pattern pattern = Pattern.compile(
                    "([^ ]*) \\(([^ ]*); ([^ ]*); ([^ ]*) ([^ ]*); ([^ ]*); ([^ ]*);\\) ([^ ]*)");
            Matcher matcher = pattern.matcher(userAgent);
            if (matcher.find()) {
                devs[0]= getMatcherGroup(matcher,6);
                devs[1]= getMatcherGroup(matcher,7);
            }
        }
        return devs;
    }
    /**
      * 方法：getArrayByPath 
      * 方法说明：    根据path获取对应的分隔数组
      * @param path
      * @return
      * @author     haoxiuzhu
      * @date       Sep 23, 2020
     */
    public static String[] getArrayByPath(String path) {
    	String[] paths= new String[3];
	    if(path != null) {
			Pattern pattern = Pattern.compile(
					"\"(\\w+) ([^\\\"]*) ([^\\\"]*)\"");
			Matcher matcher = pattern.matcher(path);
			if (matcher.find()) {
				paths[0]= getMatcherGroup(matcher,1);
				paths[1]= getMatcherGroup(matcher,2);
				paths[2]= getMatcherGroup(matcher,3);
			}
		}
		return paths;
    }
    /**
      * 方法：getTimeByLocalTime 
      * 方法说明：    根据localTime获取dateTime
      * @param localTime [23/Sep/2020:11:26:43 +0800]
      * @return 
      * @author     haoxiuzhu
      * @date       Sep 23, 2020
     */
    private static String getTimeByLocalTime(String localTime) {
		if(localTime != null) {
			Pattern pattern = Pattern.compile(
					"\\[(\\S+) \\S+\\]");
			Matcher matcher = pattern.matcher(localTime);
			if (matcher.find()) {
				return getMatcherGroup(matcher,1);
			}
		}
		return null;
	}
	/**
      * 方法：getMatcherGroup 
      * 方法说明：    获取正则group值 报错返回null
      * @param matcher
      * @param group
      * @return
      * @author     haoxiuzhu
      * @date       Sep 23, 2020
     */
	private static String getMatcherGroup(Matcher matcher, int group) {
		try {
			return matcher.group(group);
		}catch(Exception e) {
			LOGGER.error("getGroupValue has error!",e);
		}
		return null;
	}
    /** 
      * 方法：getFormatStr 
      * 方法说明：    获取格式化日期时间字符串
      * @param dateTime
      * @return
      * @author     ht-haoxiuzhu
      * @date       2019年9月17日
    */
    private static String getFormatStr(String dateTime) {
        if(dateTime != null){
            try{
                SimpleDateFormat sdf_nginx = new SimpleDateFormat(SDF_DATE_TIME_NGINX, Locale.US);
                SimpleDateFormat sdf = new SimpleDateFormat(SDF_DATE_TIME_MINUTE_SECONDS);
                return sdf.format(sdf_nginx.parse(dateTime));
            }catch(Exception e){
                LOGGER.error("getFormatStr has error!",e);
            }
        }
        return null;
    }
    /** 
     * 方法：getFormatMillStr 
     * 方法说明：    获取格式化日期时间字符串--毫秒
     * @param dateTime
     * @return
     * @author     ht-haoxiuzhu
     * @date       2019年9月17日
   */
   private static String getFormatMillStr(String dateTime) {
       if(dateTime != null){
           try{
               SimpleDateFormat sdf_nginx = new SimpleDateFormat(SDF_DATE_TIME_MM_NGINX, Locale.US);
               SimpleDateFormat sdf = new SimpleDateFormat(SDF_DATE_TIME_MINUTE_SECONDS_MM);
               return sdf.format(sdf_nginx.parse(dateTime));
           }catch(Exception e){
               LOGGER.error("getFormatStr has error!",e);
           }
       }
       return null;
   }
   public static void main(String[] args) {
//	   String logStr = "1.119.149.74 - - [23/Sep/2020:11:26:43 +0800] \"GET /tpGather/a.html?guid=70957002-208b-374a-5b6e-717370000003&appVersion=0.1.4&appPackage=com.sh.project.push.service&localtime=20200923112643263&eventType=18&chname=%E7%BA%A2%E6%B5%B7%E8%A1%8C%E5%8A%A8&packageName=com.gitvvideo.zhujiangshuma&action=com.gitvvideo.zhujiangshuma.action.ACTION_DETAIL&startflag=1&params=eyJhcHBOYW1lIjoi54ix5aWH6Im6IiwianVtcEluZm8iOnsiYWN0aW9uIjoiY29tLmdpdHZ2aWRl%0Aby56aHVqaWFuZ3NodW1hLmFjdGlvbi5BQ1RJT05fREVUQUlMIiwiZG9XaGF0Ijoic3RhcnRBY3Rp%0Adml0eSIsInBhY2thZ2VOYW1lIjoiY29tLmdpdHZ2aWRlby56aHVqaWFuZ3NodW1hIiwicGFyYW1z%0AIjoie1wicGxheUluZm9cIjpcIntcXFwidmlkZW9JZFxcXCI6XFxcIjczMzg5NTUwMFxcXCIsXFxc%0AImVwaXNvZGVJZFxcXCI6XFxcIjczMzg5NTUwMFxcXCIsXFxcImNobklkXFxcIjpcXFwiMVxcXCIs%0AXFxcImN1c3RvbWVyXFxcIjpcXFwiemh1amlhbmdzaHVtYVxcXCJ9XCJ9In0sIm5hbWUiOiLnuqLm%0AtbfooYzliqgifQ%3D%3D HTTP/1.1\" 200 0 \"-\" \"okhttp/3.11.0 (Linux; U; Android 4.4.2; Hi3798MV200; 20508;) Version/2.0.2\"";
	   String logStr ="192.168.65.59 - - [18/Nov/2021:09:43:02.492 +0800] \"GET /recGather/rec.html?guid=70927002-74ff-4c38-2a34-717150000100&appVersion=0.5.1&appPackage=com.skyworthdigital.launcher&localtime=20211118094911563&eventType=2&pid=P-WATCH-MOST&pname=%E8%BF%BD%E5%89%A7%E6%A8%A1%E5%BC%8F&vid=sh-iqy_02_04_240036501&vname=%E5%86%B3%E8%83%9C%E6%B3%95%E5%BA%AD&action=com.gitvvideo.zhujiangshuma.action.ACTION_DETAIL&params=eyJwbGF5SW5mbyI6eyJjdXN0b21lciI6InpodWppYW5nc2h1bWEiLCJjaG5JZCI6IjIiLCJlcGlz%0Ab2RlSWQiOiIyNDAwMzY1MDEiLCJ2aWRlb0lkIjoiMjQwMDM2NTAxIn19&packageName=com.gitvvideo.zhujiangshuma&source=%E7%88%B1%E5%A5%87%E8%89%BA HTTP/1.1\" 200 11 \"-\" \"okhttp/3.11.0 (Linux; U; Android 4.4.2; Hi3798MV200; 20510;) Version/2.0.2\" \"10.216.128.248\"";
	   System.out.println(getAccessLogPlus(logStr).toJSONString());
   }
}
