package com.sh.utils.file.nginx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.sh.utils.file.SecretUtil;

/**
  * 类名：NginxGetUtil.java
  * 类说明： 
  * Copyright: Copyright (c) 2012-2019
  * Company: HT
  * @author     ht-haoxiuzhu
  * @date       2019年9月16日
  * @version    1.0
*/
public class NginxGetUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(NginxGetUtil.class);
    /** 
      * 方法：getRequestParam 
      * 方法说明：    获取get请求的param值
      * @param params
      * @author     ht-haoxiuzhu
      * @date       2019年9月16日
    */
    static void getRequestParam( JSONObject json,String request) {
        if(request != null){
            request = handleParamsEncrypt(request);
            String[] strSplits  = request.split("\\?");
            if(strSplits.length >=2){
                String url = strSplits[0];
                json.put("url", url);
                String params = strSplits[1];
                if(params != null){
                    String[] paramsSplits = params.split("&");
                    String key = null;
                    String value = null;
                    for(String str:paramsSplits){
                        if(str.indexOf("=")!=-1){
                            key =str.substring(0,str.indexOf("="));
                            value = str.substring(str.indexOf("=")+1);
                        }
                        if(key != null){
                            if("api".equals(key)){
                                json.put(key, SecretUtil.base64Decode(value));
                            }else if("params".equals(key)||"condition".equals(key)){
                                json.put(key, getParamsJsonValue(handleSpecialStr(SecretUtil.base64Decode(value))));
                            }else{
                                json.put(key, SecretUtil.urlDecodeTrunk(value));
                            }
                        }
                    }
                }
            }
        }
    }
    /**
      * 方法：handleParamsEncrypt 
      * 方法说明：    处理params参数的加密处理
      * @param request
      * @return
      * @author     ht-haoxiuzhu
      * @date       2019年10月21日
     */
    private static String handleParamsEncrypt(String request) {
        String startStr = "params={";
        String endStr = "}&";
        if(request.indexOf(startStr)!=-1){
            String temp = request.substring(request.indexOf(startStr)+startStr.length()-1, request.indexOf(endStr)+1);
            StringBuilder sbr = new StringBuilder();
            sbr.append(request.substring(0,request.indexOf(temp)));
            if(!"".equals(temp)&&!"null".equals(temp)){
                sbr.append(SecretUtil.base64Encode(temp));
            }
            sbr.append(request.substring(request.indexOf(temp)+temp.length()));
            request = sbr.toString();
        }
        return request;
    }
    /***
      * 方法：handleSpecialStr 
      * 方法说明：    处理特殊字符
      * @param str
      * @return
      * @author     ht-haoxiuzhu
      * @date       2019年10月21日
     */
    private static String handleSpecialStr(String str) {
        if(str != null){
            try{
                return str.replaceAll("\\\\x5C", "").replaceAll("\\\\\\\"", "\"").replaceAll("\\\"", "\"").replaceAll("\"\\{", "{").replaceAll("\\}\"", "}");
            }catch(Exception e){
                LOGGER.error(str+",",e);
            }
        }
        return str;
    }
    /**
      * 方法：getParamsJsonValue 
      * 方法说明：    获取params值 如果是json对象则转json,否则返回原值
      * @param str
      * @return
      * @author     ht-haoxiuzhu
      * @date       2019年10月21日
     */
    private static Object getParamsJsonValue(String str) {
        if(str!= null){
            try{
                return JSONObject.parse(str);
            }catch(Exception e){
                LOGGER.error(str+",not a json!",e);
            }
        }
        return str;
    }
}
