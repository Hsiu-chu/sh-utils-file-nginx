package com.sh.utils.file.nginx;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.sh.utils.file.SecretUtil;


/**
  * 类名：NginxUtil.java
  * 类说明： nginx帮助类
  * Copyright: Copyright (c) 2012-2019
  * Company: HT
  * @author     ht-haoxiuzhu
  * @date       2019年9月16日
  * @version    1.0
*/
public class TomcatUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(TomcatUtil.class);
    public static final String SDF_DATE_TIME_MINUTE_SECONDS_MM = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String SDF_DATE_TIME_MM_NGINX = "dd/MMM/yyyy:HH:mm:ss.SSS";
    public static final String SDF_DATE_TIME_MINUTE_SECONDS = "yyyy-MM-dd HH:mm:ss";
    public static final String SDF_DATE_TIME_NGINX = "dd/MMM/yyyy:HH:mm:ss";
    /**
      * 方法：getAccessLogPlus 
      * 方法说明：   正则解析并获取access日志信息
      * @param lineStr
      * @return
      * @author     haoxiuzhu
      * @date       Sep 23, 2020
     */
    public static JSONObject getAccessLogPlus(String lineStr) {
        if(lineStr != null){
           try {
        	  Pattern pattern = Pattern.compile(
       				"([^ ]*) ([^ ]*) ([^ ]*) (\\[.*\\]) (\\\".*?\\\") (-|[0-9]*) (-|[0-9]*)");
       	      Matcher matcher = pattern.matcher(lineStr);
              JSONObject json = new JSONObject();
              if (matcher.find()) {
           	   String ip = getMatcherGroup(matcher,1);//1.119.149.74
                  json.put("ip", ip);
                  
                  String localTime = getMatcherGroup(matcher,4);//[23/Sep/2020:11:26:43 +0800]
                  String dateTime = getTimeByLocalTime(localTime);
                  String dateTimeFormat = null;
                  if(dateTime.indexOf(".")!=-1) {
               	   dateTimeFormat =  getFormatMillStr(dateTime);
                  }else {
               	   dateTimeFormat =  getFormatStr(dateTime);
                  }
                  json.put("dateTime",dateTimeFormat);
                  if(dateTimeFormat != null && dateTimeFormat.length()>10){
                      json.put("dateDay",dateTimeFormat.subSequence(0, 10));
                  }
                  
                  String status =  getMatcherGroup(matcher,6);//200
                  json.put("status", status);
                  
                  long duration = getDuration(getMatcherGroup(matcher,7));//"okhttp/3.11.0 (Linux; U; Android 4.4.2; Hi3798MV200; 20508;) Version/2.0.2"
                  json.put("duration", duration);//单位:ms或者ms 对应%D或%T
                  
                  String path = getMatcherGroup(matcher,5);//"POST /emergency-api/emergency/heartbeat HTTP/1.1"
                  if(path != null) {
               	   String[] pathArray = getArrayByPath(path);
               	   if(pathArray.length>=3) {
               		   String mothod = pathArray[0];
          				   json.put("mothod", mothod);
              			   String request = SecretUtil.urlDecodeTrunk(pathArray[1]);
                          json.put("request",request);
                          if("GET".equals(mothod)){
                              NginxGetUtil.getRequestParam(json,request);
                          }
               	   }
                  }
              }
              return json;
           }catch(Exception e) {
        	   LOGGER.error("getAccessLogPlus has error!",e);
           }
        }
        return null;
    }
    /**
      * 方法：getDuration 
      * 方法说明：    获取时长
      * @param matcherGroup
      * @return 单位:ms或者ms 对应%D或%T
      * @author     haoxiuzhu
      * @date       Sep 24, 2020
     */
    private static long getDuration(String matcherGroup) {
    	if(matcherGroup != null) {
    		try {
    			return Long.parseLong(matcherGroup);
    		}catch(Exception e) {
    			LOGGER.error("getDuration has error!",e);
    		}
    	}
		return 0;
	}
	/**
      * 方法：getArrayByPath 
      * 方法说明：    根据path获取对应的分隔数组
      * @param path
      * @return
      * @author     haoxiuzhu
      * @date       Sep 23, 2020
     */
    public static String[] getArrayByPath(String path) {
    	String[] paths= new String[3];
	    if(path != null) {
			Pattern pattern = Pattern.compile(
					"\"(\\w+) ([^\\\"]*) ([^\\\"]*)\"");
			Matcher matcher = pattern.matcher(path);
			if (matcher.find()) {
				paths[0]= getMatcherGroup(matcher,1);
				paths[1]= getMatcherGroup(matcher,2);
				paths[2]= getMatcherGroup(matcher,3);
			}
		}
		return paths;
    }
    /**
      * 方法：getTimeByLocalTime 
      * 方法说明：    根据localTime获取dateTime
      * @param localTime [23/Sep/2020:11:26:43 +0800]
      * @return 
      * @author     haoxiuzhu
      * @date       Sep 23, 2020
     */
    private static String getTimeByLocalTime(String localTime) {
		if(localTime != null) {
			Pattern pattern = Pattern.compile(
					"\\[(\\S+) \\S+\\]");
			Matcher matcher = pattern.matcher(localTime);
			if (matcher.find()) {
				return getMatcherGroup(matcher,1);
			}
		}
		return null;
	}
	/**
      * 方法：getMatcherGroup 
      * 方法说明：    获取正则group值 报错返回null
      * @param matcher
      * @param group
      * @return
      * @author     haoxiuzhu
      * @date       Sep 23, 2020
     */
	private static String getMatcherGroup(Matcher matcher, int group) {
		try {
			return matcher.group(group);
		}catch(Exception e) {
			LOGGER.error("getGroupValue has error!",e);
		}
		return null;
	}
    /** 
      * 方法：getFormatStr 
      * 方法说明：    获取格式化日期时间字符串
      * @param dateTime
      * @return
      * @author     ht-haoxiuzhu
      * @date       2019年9月17日
    */
    private static String getFormatStr(String dateTime) {
        if(dateTime != null){
            try{
                SimpleDateFormat sdf_nginx = new SimpleDateFormat(SDF_DATE_TIME_NGINX, Locale.US);
                SimpleDateFormat sdf = new SimpleDateFormat(SDF_DATE_TIME_MINUTE_SECONDS);
                return sdf.format(sdf_nginx.parse(dateTime));
            }catch(Exception e){
                LOGGER.error("getFormatStr has error!",e);
            }
        }
        return null;
    }
    /** 
     * 方法：getFormatMillStr 
     * 方法说明：    获取格式化日期时间字符串--毫秒
     * @param dateTime
     * @return
     * @author     ht-haoxiuzhu
     * @date       2019年9月17日
   */
   private static String getFormatMillStr(String dateTime) {
       if(dateTime != null){
           try{
               SimpleDateFormat sdf_nginx = new SimpleDateFormat(SDF_DATE_TIME_MM_NGINX, Locale.US);
               SimpleDateFormat sdf = new SimpleDateFormat(SDF_DATE_TIME_MINUTE_SECONDS_MM);
               return sdf.format(sdf_nginx.parse(dateTime));
           }catch(Exception e){
               LOGGER.error("getFormatStr has error!",e);
           }
       }
       return null;
   }
   public static void main(String[] args) {
	   String logStr = "182.51.86.200 - - [24/Sep/2020:13:13:03 +0800] \"POST /emergency-api/emergency/heartbeat HTTP/1.1\" 200 23";
	   System.out.println(getAccessLogPlus(logStr).toJSONString());
   }
}
