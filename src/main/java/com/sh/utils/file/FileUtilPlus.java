package com.sh.utils.file;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.json.CDL;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.sh.utils.file.nginx.NginxUtil;
import com.sh.utils.ics.ICSUtil;

/**
  * 类名：FileUtilPlus.java
  * 类说明： 写csv文件 
  * Copyright: Copyright (c) 2012-2020
  * Company: HT
  * @author     ht-haoxiuzhu
  * @date       2020年8月20日
  * @version    1.0
*/
public class FileUtilPlus {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileUtilPlus.class);
    /** 
     * 用200M的缓冲读取文本文件  
     */
     public static final int BUFFER_MAX = 200 * 1024 * 1024;

     /**
       * 方法：readLineFile 
       * 方法说明：    逐行读取文件
       * @param filePath 文件路径
       * @param switch_split_hb 是否开启心跳分离开关 true:开启
       * @author     ht-haoxiuzhu
       * @date       2019年9月16日
      */
     public static void readLineFile(String filePath,boolean switch_split_hb) {
         LOGGER.info("filePath:{},SWITCH_SPLIT_HB:{}",filePath,switch_split_hb);
         if (filePath != null) {
             File file = null;
             BufferedInputStream fis = null;
             BufferedReader reader = null;
             try {
                 file = new File(filePath);
                 if (file != null && file.isFile()) {
                     fis = new BufferedInputStream(new FileInputStream(file));
                     reader = new BufferedReader(new InputStreamReader(fis,"utf-8"), BUFFER_MAX);
                     String line = "";
                     JSONObject json = null;
                     String eventType = null;
                     org.json.JSONArray ja = null;
                     org.json.JSONObject jo = null;
                     while ((line = reader.readLine()) != null) {
                       json =  NginxUtil.getAccessLogPlus(line);
                       if(json != null){
                    	   eventType = json.getString("eventType");
                    	   ja = new org.json.JSONArray();
                    	   ja.put(json);
                    	   jo = ja.optJSONObject(0);
                    	   if (jo != null && eventType != null) {
                               JSONArray names = jo.names();
//                               LOGGER.info(CDL.rowToString(names)+",eventType="+eventType);
                               writeLogFileForStr(file.getParent(),"E"+eventType,CDL.toString(names,ja));
                           }
                    	  
                       }
                     }
                 }
             } catch (Exception e) {
                 e.printStackTrace();
             } finally {
                 if (reader != null) {
                     try {
                         reader.close();
                     } catch (IOException e) {
                         e.printStackTrace();
                     }
                 }
                 if (fis != null) {
                     try {
                         fis.close();
                     } catch (IOException e) {
                         e.printStackTrace();
                     }
                 }
             }

         }
     }
     /**
       * 方法：writeLogFileForStr 
       * 方法说明：    行字符串写入到日志文件
       * @param file
       * @param lineStr
       * @author     ht-haoxiuzhu
       * @date       2019年10月18日
      */
    private static void writeLogFileForStr(String sourceFileParentPath,String sourceFileName, String lineStr) {
    	if(lineStr != null) {
    		String fileName = ICSUtil.getLogFileName(sourceFileParentPath+"/nginx",sourceFileName, ".csv",500*1024*1024);
            ICSUtil.writeLogFile(fileName,lineStr);
    	}
    }
     /***
       * 方法：readFile 
       * 方法说明：    根据目录读log文件转写问json文件
       * @param path
       * @param switch_split 是否开启分离开关 true:开启
       * @author     ht-haoxiuzhu
       * @date       2019年9月16日
      */
     public static void readFile(String path,boolean switch_split){
         LOGGER.info("path:{}",path);
         if(path!= null){
             File fileDir = new File(path);
             if(fileDir.isDirectory()){
                 for(File file: fileDir.listFiles()){
                     if(file.isFile() && file.getName().endsWith(".log")){
                         long startTime = System.currentTimeMillis();
                         readLineFile(file.getAbsolutePath(),switch_split);
                         long endTime = System.currentTimeMillis();
                         LOGGER.info(file.getAbsolutePath()+",readTime:"+(endTime-startTime));
                         ICSUtil.moveFileToPath(file.getAbsolutePath(), file.getParent()+"/readFile/"); 
                         LOGGER.info(file.getAbsolutePath()+",moveTime:"+(System.currentTimeMillis()-endTime));
                     }
                 }
             }
         }
     }
}
