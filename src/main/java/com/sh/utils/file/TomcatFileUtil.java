package com.sh.utils.file;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.sh.utils.file.nginx.TomcatUtil;
import com.sh.utils.ics.ICSUtil;

/**
  * 类名：TomcatFileUtil.java
  * 类说明： tomcat localhost文件分析
  * Copyright: Copyright (c) 2012-2019
  * Company: HT
  * @author     ht-haoxiuzhu
  * @date       2019年9月16日
  * @version    1.0
*/
public class TomcatFileUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(TomcatFileUtil.class);
    /** 
     * 用200M的缓冲读取文本文件  
     */
     public static final int BUFFER_MAX = 200 * 1024 * 1024;

     /**
       * 方法：readLineFile 
       * 方法说明：    逐行读取文件
       * @param filePath 文件路径
       * @author     ht-haoxiuzhu
       * @date       2019年9月16日
      */
     public static void readLineFile(String filePath) {
         LOGGER.info("filePath:{}",filePath);
         if (filePath != null) {
             File file = null;
             BufferedInputStream fis = null;
             BufferedReader reader = null;
             try {
                 file = new File(filePath);
                 if (file != null && file.isFile()) {
                     fis = new BufferedInputStream(new FileInputStream(file));
                     reader = new BufferedReader(new InputStreamReader(fis,"utf-8"), BUFFER_MAX);
                     String line = "";
                     long count = 0L;
                     StringBuilder sbf = new StringBuilder();
                     JSONObject json = null;
                     while ((line = reader.readLine()) != null) {
                       json =  TomcatUtil.getAccessLogPlus(line);
                       if(json != null){
                    	   sbf.append(json.toJSONString()+"\n");
                           if(count!=0 && count%2000 == 0){
                        	   writeLogFileForStr(file.getParent(),file.getName(),sbf.toString());
                               sbf = new StringBuilder();
                           }
                           count++;
                       }
                     }
                     LOGGER.info("sbf.length:{}",sbf.length());
                     if(sbf.length()>0){
                         writeLogFileForStr(file.getParent(),file.getName(),sbf.toString());
                     }
                     LOGGER.info("sum count:{}",count);
                 }
             } catch (Exception e) {
            	 LOGGER.error("readLineFile has error!",e);
             } finally {
                 if (reader != null) {
                     try {
                         reader.close();
                     } catch (IOException e) {
                         e.printStackTrace();
                     }
                 }
                 if (fis != null) {
                     try {
                         fis.close();
                     } catch (IOException e) {
                         e.printStackTrace();
                     }
                 }
             }

         }
     }
     /**
       * 方法：writeLogFileForStr 
       * 方法说明：    行字符串写入到日志文件
       * @param file
       * @param lineStr
       * @author     ht-haoxiuzhu
       * @date       2019年10月18日
      */
    private static void writeLogFileForStr(String sourceFileParentPath,String sourceFileName, String lineStr) {
        long startTime = System.currentTimeMillis();
        String fileName = ICSUtil.getLogFileName(sourceFileParentPath+"/localhost",sourceFileName, ".json",200*1024*1024);
        long endTime = System.currentTimeMillis();
        ICSUtil.writeLogFile(fileName,lineStr);
        long end2Time = System.currentTimeMillis();
        LOGGER.debug("find time:"+(endTime-startTime)+",writeLog time:"+(end2Time-endTime)+",fileName:"+fileName);
    }
     /***
       * 方法：readFile 
       * 方法说明：    根据目录读log文件转写问json文件
       * @param path
       * @author     ht-haoxiuzhu
       * @date       2019年9月16日
      */
     public static void readFile(String path){
         LOGGER.info("path:{}",path);
         if(path!= null){
             File fileDir = new File(path);
             if(fileDir.isDirectory()){
                 for(File file: fileDir.listFiles()){
                	 LOGGER.info("fileName:{}",file.getName());
                     if(file.isFile() && file.getName().endsWith(".txt")){
                         long startTime = System.currentTimeMillis();
                         readLineFile(file.getAbsolutePath());
                         long endTime = System.currentTimeMillis();
                         LOGGER.info(file.getAbsolutePath()+",readTime:"+(endTime-startTime));
                         ICSUtil.moveFileToPath(file.getAbsolutePath(), file.getParent()+"/readFile/"); 
                         LOGGER.info(file.getAbsolutePath()+",moveTime:"+(System.currentTimeMillis()-endTime));
                     }
                 }
             }
         }
     }
}
