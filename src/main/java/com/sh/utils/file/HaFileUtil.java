package com.sh.utils.file;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.sh.utils.file.nginx.HaUtil;
import com.sh.utils.file.nginx.NginxErrorUtil;
import com.sh.utils.file.nginx.NginxUtil;
import com.sh.utils.ics.ICSUtil;

/**
  * 类名：FileNginx.java
  * 类说明： 
  * Copyright: Copyright (c) 2012-2019
  * Company: HT
  * @author     ht-haoxiuzhu
  * @date       2019年9月16日
  * @version    1.0
*/
public class HaFileUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(HaFileUtil.class);
    /** 用200M的缓冲读取文本文件  */
     public static final int BUFFER_MAX = 200 * 1024 * 1024;
     /***上报数据的业务app包名 */
     public static final String APP_PACKAGE_LAUNCER = "com.skyworthdigital.launcher";
     
     /** 心跳eventType值 1*/
     public static final String EVENT_TYPE_L_HB = "1";
     /** item 点击 eventType值 13*/
     public static final String EVENT_TYPE_L_ITEM_CLICK = "13";
     /** item 聚焦 eventType值 14*/
     public static final String EVENT_TYPE_L_ITEM_FOCUS = "14";
     /** 状态栏 点击 eventType值 16*/
     public static final String EVENT_TYPE_L_BAR_CLICK = "16";
     /** 直播自动进入 点击eventType值 21*/
     public static final String EVENT_TYPE_L_LIVE = "21";
     /** 直播 逻辑频道号 进入eventType值 22*/
     public static final String EVENT_TYPE_L_LIVE_LOGIC= "22";
     /*** 心跳分开写文件的开关值 true:开 false:关*/
     public static final boolean SWITCH_SPLIT_HB = true;
     
     
     /**直播 包名*/
     public static final String APP_PACKAGE_LIVE = "com.skyworthdigital.stb.app";
     /**配置 包名*/
     public static final String APP_PACKAGE_SETTINGS = "com.skyworthdigital.settings";
     /**个人中心 包名*/
     public static final String APP_PACKAGE_PERSONAL = "com.sh.project.personal.center";
     /**搜索 包名*/
     public static final String APP_PACKAGE_SEARCH = "com.sh.project.global.search";

     /**
       * 方法：readLineFile 
       * 方法说明：    逐行读取文件
       * @param filePath 文件路径
       * @param notError true:非error日志,为access日志
       * @author     ht-haoxiuzhu
       * @date       2019年9月16日
      */
     public static void readLineFile(String filePath,boolean notError) {
         LOGGER.info("filePath:{},notError:{}",filePath,notError);
         if (filePath != null) {
             File file = null;
             BufferedInputStream fis = null;
             BufferedReader reader = null;
             try {
                 file = new File(filePath);
                 if (file != null && file.isFile()) {
                     fis = new BufferedInputStream(new FileInputStream(file));
                     reader = new BufferedReader(new InputStreamReader(fis,"utf-8"), BUFFER_MAX);
                     String line = "";
                     long count = 0L;
                     StringBuilder sbf = new StringBuilder();
                     long countHB = 0L;
                     StringBuilder sbfHB = new StringBuilder();
                     JSONObject json = null;
                     String eventType =  null;
                     while ((line = reader.readLine()) != null) {
                       json =  HaUtil.getAccessLogPlus(line);
                       eventType = json.getString("eventType");//重要属性 不可缺失
                       if(json != null && eventType != null){
                    	   if(count!=0 && count%2000 == 0){
                         	   writeLogFileForStr(file.getParent(),file.getName(),sbf.toString());
                                sbf = new StringBuilder();
                            }
                     	   if(countHB!=0 && countHB%2000 == 0){
                         	   writeLogFileForStr(file.getParent(),"HB_"+file.getName(), sbfHB.toString());
                         	   sbfHB = new StringBuilder();
                            }
                    	   String appPackage = json.getString("appPackage");//业务属性 做具体的业务处理---补数据
                      	   if(APP_PACKAGE_LAUNCER.equals(appPackage)) {
                      		   if(!EVENT_TYPE_L_HB.equals(eventType)) {
                      			   handleLauncherData(json,eventType);
                      			   sbf.append(json.toJSONString()+"\n");
                                   count++;
                      		   }else {//launcher的心跳默认分开不再由外部参数控制
                      			   sbfHB.append(json.toJSONString()+"\n");
                        		   countHB++;
                      		   }
                      	   }else {
                      		   sbf.append(json.toJSONString()+"\n");
                      		   count++;
                      	   }
                       }
                     }
                     LOGGER.info("sbfHB.length:{}",sbfHB.length());
                     if(sbfHB.length()>0){
                         writeLogFileForStr(file.getParent(),"HB_"+file.getName(), sbfHB.toString());
                         sbfHB = new StringBuilder();
                     }
                     LOGGER.info("HB sum count:{}",countHB);
                     
                     LOGGER.info("sbf.length:{}",sbf.length());
                     if(sbf.length()>0){
                         writeLogFileForStr(file.getParent(),file.getName(),sbf.toString());
                         sbf = new StringBuilder();
                     }
                     LOGGER.info("sum count:{}",count);
                 }
             } catch (Exception e) {
            	 LOGGER.error("readLineFile has error!",e);
             } finally {
                 closeStream(fis, reader);
             }

         }
     }
     /**
       * 方法：closeStream 
       * 方法说明： 关闭流
       * @param fis
       * @param reader
       * @author     haoxiuzhu
       * @date       Oct 14, 2020
      */
	private static void closeStream(BufferedInputStream fis, BufferedReader reader) {
		if (reader != null) {
		     try {
		         reader.close();
		     } catch (IOException e) {
		         e.printStackTrace();
		     }
		 }
		 if (fis != null) {
		     try {
		         fis.close();
		     } catch (IOException e) {
		         e.printStackTrace();
		     }
		 }
	}
	/**
       * 方法：handleLauncherData 
       * 方法说明： 处理launcher的业务数据---补全数据
       *        apk由于版本问题缺失了必要数据 可以由此次补全
       * @param json
       * @param eventType
       * @author     haoxiuzhu
       * @date       Oct 14, 2020
      */
     private static void handleLauncherData(JSONObject json,String eventType) {
    	try {
    		switch(eventType) {
			case EVENT_TYPE_L_ITEM_CLICK:
			case EVENT_TYPE_L_ITEM_FOCUS:
				handleLauncherPannel(json);
				break;
			case EVENT_TYPE_L_BAR_CLICK:
				handleLauncherBar(json);
				break;
			case EVENT_TYPE_L_LIVE:
			case EVENT_TYPE_L_LIVE_LOGIC:
				handleLauncherLive(json);
				break;
    		}
    	}catch(Exception e) {
    		LOGGER.error(eventType,e);
    	}
	}
    /**
      * 方法：handleLauncherPannel 
      * 方法说明：    补全pannel的pid
      * @param json
      * @author     haoxiuzhu
      * @date       Oct 14, 2020
     */
	private static void handleLauncherPannel(JSONObject json) {
		String pid = json.getString("pid");
		if(pid == null || "".equals(pid.trim())) {
			String pname = json.getString("pname");
			if(pname == null) {
			   return;
			}
			switch(pname) {
				case "我的推荐位":
					json.put("pid","my_features");
					break;
				case "常用功能":
					json.put("pid","commonly_used_entrances");
					break;
				case "我的应用":
					json.put("pid","my_applications");
					break;
				case "播放历史":
					json.put("pid","my_histories");
					break;
				case "收藏记录":
					json.put("pid","my_favorites");
					break;
				case "更多":
					json.put("pid","more_entrances");
					break;
				case "我的活动":
					json.put("pid","my_activities");
					break;
			}
		}
		
	}
	/**
	  * 方法：handleLauncherBar 
	  * 方法说明：    补全bar应用包名
	  * @param json
	  * @author     haoxiuzhu
	  * @date       Oct 14, 2020
	 */
	private static void handleLauncherBar(JSONObject json) {
		String packageName = json.getString("packageName");
		if(packageName == null || "".equals(packageName.trim())) {
			String barname = json.getString("barname");
			switch(barname) {
				case "网络设置":
					json.put("packageName",APP_PACKAGE_SETTINGS);
					break;
				case "历史":
					json.put("packageName",APP_PACKAGE_PERSONAL);
					break;
				case "搜索":
					json.put("packageName",APP_PACKAGE_SEARCH);
					break;
			}
		}
	}
	/**
	  * 方法：handleLauncherLive 
	  * 方法说明：    补全直播应用包名
	  * @param json
	  * @author     haoxiuzhu
	  * @date       Oct 14, 2020
	 */
	private static void handleLauncherLive(JSONObject json) {
		String packageName = json.getString("packageName");
		if(packageName == null || "".equals(packageName.trim())) {
			json.put("packageName", APP_PACKAGE_LIVE);
		}
		
	}
	/**
       * 方法：writeLogFileForStr 
       * 方法说明：    行字符串写入到日志文件
       * @param file
       * @param lineStr
       * @author     ht-haoxiuzhu
       * @date       2019年10月18日
      */
    private static void writeLogFileForStr(String sourceFileParentPath,String sourceFileName, String lineStr) {
        long startTime = System.currentTimeMillis();
        String fileName = ICSUtil.getLogFileName(sourceFileParentPath+"/nginx",sourceFileName, ".json",200*1024*1024);
        long endTime = System.currentTimeMillis();
        ICSUtil.writeLogFile(fileName,lineStr);
        long end2Time = System.currentTimeMillis();
        LOGGER.debug("find time:"+(endTime-startTime)+",writeLog time:"+(end2Time-endTime)+",fileName:"+fileName);
    }
     /***
       * 方法：readFile 
       * 方法说明：    根据目录读log文件转写问json文件
       * @param path
       * @param switch_split 是否开启分离开关 true:开启
       * @param notError true:非error日志,为access日志
       * @author     ht-haoxiuzhu
       * @date       2019年9月16日
      */
     public static void readFile(String path,boolean switch_split,boolean notError){
         LOGGER.info("path:{},switch_split:{},notError:{}",path,switch_split,notError);
         if(path!= null){
             File fileDir = new File(path);
             if(fileDir.isDirectory()){
                 for(File file: fileDir.listFiles()){
                     if(file.isFile() && file.getName().endsWith(".log")){
                         long startTime = System.currentTimeMillis();
                         readLineFile(file.getAbsolutePath(),notError);
                         long endTime = System.currentTimeMillis();
                         LOGGER.info(file.getAbsolutePath()+",readTime:"+(endTime-startTime));
                         ICSUtil.moveFileToPath(file.getAbsolutePath(), file.getParent()+"/readFile/"); 
                         LOGGER.info(file.getAbsolutePath()+",moveTime:"+(System.currentTimeMillis()-endTime));
                     }
                 }
             }
         }
     }
}
