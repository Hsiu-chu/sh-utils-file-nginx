package com.sh.utils.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
  * 类名：Lancher.java
  * 类说明： 主启动类
  * Copyright: Copyright (c) 2012-2020
  * Company: HT
  * @author     haoxiuzhu
  * @date       Sep 24, 2020
  * @version    1.0
 */
public class Lancher {
	private static final Logger LOGGER = LoggerFactory.getLogger(Lancher.class);
	/**正常nginx日志输出csv文件为csv*/
    public static final String TYPE_CSV = "csv";
    /**正常tomcat日志输出json文件为tomcat*/
    public static final String TYPE_TOMCAT = "tomcat";
    /**异常nginx日志输出json文件为error*/
    public static final String TYPE_ERROR = "error";
    /**正常nginx日志输出json文件为nginx*/
    public static final String TYPE_NGINX = "nginx";
    /**正常HA日志输出json文件为HA*/
    public static final String TYPE_HA = "HA";
	/** 
      * 方法：main 
      * 方法说明：    主入口
      * @param args
      * @author     ht-haoxiuzhu
      * @date       2019年9月16日
    */
    public static void main(String[] args) {
    	String type =TYPE_NGINX;//正常nginx日志输出json文件为nginx-默认值
    	String switchSplit="true";//心跳分开写文件的开关打开-默认值
    	String path = "/Users/haoxiuzhu/Downloads/access_log/";//log文件读取目录-默认值
    	boolean notError = true;//非error日志，正常的日志标识
    	if(args.length>2) {
    		type = args[2];
    		switchSplit = args[1];
    		path = args[0];
    	}else if(args.length>1){
    		switchSplit = args[1];
    		path = args[0];
        }else if(args.length>0){
        	path = args[0];
        }
    	LOGGER.info("type:{},switchSplit:{},path:{}",type,switchSplit,path);
		if(TYPE_NGINX.equals(type)) { 
			FileUtil.readFile(path,getSwitchSplit(switchSplit),notError);
		}else if(TYPE_HA.equals(type)) { 
			HaFileUtil.readFile(path,getSwitchSplit(switchSplit),notError);
		}else if(TYPE_CSV.equals(type)) {
			FileUtilPlus.readFile(path,getSwitchSplit(switchSplit));
		}else if(TYPE_TOMCAT.equals(type)) {
			TomcatFileUtil.readFile(path);
		}else if(TYPE_ERROR.equals(type)) {
			notError = false;
			FileUtil.readFile(path,getSwitchSplit(switchSplit),notError);
		}else {
			FileUtil.readFile(path,getSwitchSplit(switchSplit),notError);
		}
    }
    /**
     * 
      * 方法：getSwitchSplit 
      * 方法说明：    根据输入参数获取分离开关值
      * @param switchSplit
      * @return
      * @author     haoxiuzhu
      * @date       Aug 7, 2020
     */
	private static boolean getSwitchSplit(String switchSplit) {
		if(switchSplit == null || "".endsWith(switchSplit.trim())) {
			return FileUtil.SWITCH_SPLIT_HB;
		}
		try {
			return Boolean.parseBoolean(switchSplit);
		}catch(Exception e) {
			return FileUtil.SWITCH_SPLIT_HB;
		}
	}

}
