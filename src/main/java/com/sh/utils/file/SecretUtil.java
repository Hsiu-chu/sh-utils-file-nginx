package com.sh.utils.file;

import java.net.URLDecoder;
import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sh.utils.encrypt.Base64Utils;

/**
  * 类名：SecretUtil.java
  * 类说明： 加密解密帮助类
  * Copyright: Copyright (c) 2012-2019
  * Company: HT
  * @author     ht-haoxiuzhu
  * @date       2019年10月21日
  * @version    1.0
*/
public class SecretUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(SecretUtil.class);
    /**
     * 方法：urlDecode 
     * 方法说明：    url解密，不对则返回原串---该接口后续将作废
     * @param keyValue
     * @return
     * @author     ht-haoxiuzhu
     * @date       2019年10月21日
    */
   public static String urlDecode(String str) {
       if(str!= null){
           try{
               return URLDecoder.decode(str,"UTF-8").replaceAll(" ", "+");
           }catch(Exception e){
               LOGGER.error("urlDecode has error!",e);
           }
       }
       return str;
   }
   /**
     * 方法：urlDecodeTrunk 
     * 方法说明： url解密，不对则返回原串---该接口为正确接口
     *           不再兼容非法字符未加密等问题
     * @param str
     * @return
     * @author     haoxiuzhu
     * @date       Sep 17, 2020
    */
   public static String urlDecodeTrunk(String str) {
       if(str!= null){
           try{
               return URLDecoder.decode(str,"UTF-8");
           }catch(Exception e){
               LOGGER.error("urlDecode has error!ex={},str:{}",e,str);
           }
       }
       return str;
   }
   /***
     * 方法：urlEncode 
     * 方法说明：   url加密
     * @param str
     * @return
     * @author     ht-haoxiuzhu
     * @date       2019年10月21日
    */
   public static String urlEncode(String str) {
       if(str!= null){
           try{
               return URLEncoder.encode(str,"UTF-8");
           }catch(Exception e){
               LOGGER.error("urlEncode has error!",e);
           }
       }
       return str;
   }
   /**
     * 方法：base64Encode 
     * 方法说明：    base64加密
     * @param str
     * @return
     * @author     ht-haoxiuzhu
     * @date       2019年10月21日
    */
   public static String base64Encode(String str) {
       if(str!= null){
           try{
               return Base64Utils.encode(str);
           }catch(Exception e){
               LOGGER.error("base64Encode has error!",e);
           }
       }
       return str;
   }
   /**
     * 方法：base64Decode 
     * 方法说明：    base64解密，不对则返回原串
     * @param str
     * @return
     * @author     ht-haoxiuzhu
     * @date       2019年10月21日
    */
   public static String base64Decode(String str) {
       if(str!= null && !"null".equals(str)){
           try{
               return Base64Utils.decodeStr(str);
           }catch(Exception e){
               LOGGER.error(str+",base64Decode has error!",e);
           }
       }
       return str;
   }
   public static void main(String args[]) {
	   String str1 = "GET /portalGather/a.html?localtime=20200316235232&packageName=com.gitvvideo.zhujiangshuma&rom=20502&areaCode=Y01P0001&doWhat=startActivity&pid=c9888a018ec844099395504b6c565875&isTest=0&params=eyJwbGF5SW5mbyI6IntsaXN0VHlwZTpcImNoYW5uZWxcIixjaG5JZDpcIjE1XCIsY2huTmFtZTpcIuWEv+erpVwiLFwiY3VzdG9tZXJcIjpcInpodWppYW5nc2h1bWFcIn0ifQ==&code=qiyi&eventType=5&type=1&appPackage=com.skyworthdigital.launcher&id=520200749&guid=70567002-0c49-3370-1627-717290022000&title=&appVersion=0.3.3&pageTitle=%E7%88%B1%E5%A5%87%E8%89%BA&name=&seq=6&action=com.gitvvideo.zhujiangshuma.action.ACTION_ALBUMLIST&pname=%E7%88%B1%E5%A5%87%E8%89%BA";
	   System.out.println(urlDecode(str1));
	   System.out.println(urlDecodeTrunk(str1));
	   String str2 = "eyJwbGF5SW5mbyI6IntsaXN0VHlwZTpcImNoYW5uZWxcIixjaG5JZDpcIjE1XCIsY2huTmFtZTpcIuWEv+erpVwiLFwiY3VzdG9tZXJcIjpcInpodWppYW5nc2h1bWFcIn0ifQ==";
	   System.out.println(base64Decode(str2));
   }
}
