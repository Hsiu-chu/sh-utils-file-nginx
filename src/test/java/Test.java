import com.sh.utils.file.FileUtil;
import com.sh.utils.file.SecretUtil;
import com.sh.utils.file.nginx.HaUtil;
import com.sh.utils.file.nginx.NginxErrorUtil;

/**
  * 类名：Test.java
  * 类说明： 
  * Copyright: Copyright (c) 2012-2019
  * Company: HT
  * @author     ht-haoxiuzhu
  * @date       2019年10月18日
  * @version    1.0
*/
public class Test {

    /** 
      * 方法：main 
      * 方法说明：    TODO
      * @param args
      * @author     ht-haoxiuzhu
      * @date       2019年10月18日
    */
    public static void main(String[] args) {
        System.out.println(Math.pow(2, 10));
//        String lineStr = "2020/08/21 15:36:16 [crit] 86901#0: *5241 open() \"/launcher_ics/LICS1/access_2020-08-21-15.log\" failed (2: No such file or directory) while logging request, client: 116.199.7.37, server: localhost, request: \"GET /launcherGather/a.html?guid=70927002-7085-4057-4f33-718390143047&areaCode=Y01P0002&appVersion=0.4.3&rom=20504&appPackage=com.skyworthdigital.launcher&runtime=13809&localtime=20200821153615547&isTest=0&eventType=1&eventMode=heartbeat&runstate=bg&style=1&name=%E6%99%AE%E9%80%9A%E6%A1%8C%E9%9D%A2 HTTP/1.1\", host: \"launcherlog.candytime.com.cn\"";
//        String json = NginxErrorUtil.getAccessLog(lineStr).toJSONString();
        //
//        String lineStr = "116.199.7.37 - - [12/Jan/2021:15:59:59.999 +0800] \"GET /launcherGather/a.html?guid=70927002-6c2c-dca6-6831-718320100529&areaCode=Y01P0002&appVersion=0.4.4&rom=20508&appPackage=com.skyworthdigital.launcher&runtime=120&localtime=20210112155959945&isTest=0&eventType=14&eventMode=switch&style=1&name=%E6%99%AE%E9%80%9A%E6%A1%8C%E9%9D%A2&nid=b00a24b28f4a4dd4860432012d8dfdd8&nname=%E7%9B%B4%E6%92%AD&ncode=live&pid=a82215e476854093ab7169386beb0377&pname=%E7%9B%B4%E6%92%AD%E9%A6%96%E5%B1%8F&ptitle=%E7%9B%B4%E6%92%AD%E9%A6%96%E5%B1%8F&seq=6&iid=76e457343eaf4498847f42ffdec6bde9&iname=%E4%B8%83%E5%A4%A9%E5%9B%9E%E7%9C%8B&ititle=&itype=1&opmode=0&datasource=1 HTTP/1.1\" 200 11 \"-\" \"okhttp/3.11.0\" \"10.170.144.57\"";
        String lineStr = "Jan 11 10:50:00 localhost haproxy[7722]: 10.226.69.82:41530 [11/Jan/2021:10:50:00.015] PICM launcher_ics/192.168.44.138 8/0/1/0/9 200 151 - - ---- 17198/17198/1/1/0 0/0 \"GET /launcherGather/a.html?guid=70587002-0c49-33f0-f47c-718480075926&areaCode=Y01P0007&appVersion=0.4.4&rom=20506&appPackage=com.skyworthdigital.launcher&runtime=2070644&localtime=20210111104959132&isTest=0&eventType=1&eventMode=heartbeat&runstate=bg&style=1&name=%E9%BB%84%E5%9F%94%E6%99%AE%E9%80%9A%E6%A1%8C%E9%9D%A2 HTTP/1.1\"";
        String json = HaUtil.getAccessLogPlus(lineStr).toJSONString();
        System.out.println(json);
    }

}
